from .models import Save, Profil
from .forms import FileUploadForm
from .admin import SaveAdmin

from django.contrib import admin
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings

import os


def save(request, pk):
    try:
        # Get the profil instance from the pk
        profil_instance = Profil.objects.get(pk=pk)
        # Create a Save instance
        save_instance = Save(profil=profil_instance)
        # Call the save method to save the instance
        save_instance.save()
        # Redirect to the change list
        messages.success(request, "Save successfully created")

    except Profil.DoesNotExist:
        # Manage the case where the profil does not exist
        messages.error(request, "The profil does not exist")

    return redirect("/pyscada_admin/saves/profil/")


def refresh(request):
    # Create a Save instance
    save_instance = Save()
    # Call the refresh_from_directory method to refresh the saves
    save_instance.refresh_from_directory()
    # Redirect to the change list
    messages.success(request, "Save successfully refreshed")
    return redirect("/pyscada_admin/saves/save/")


def load(request, pk):
    # Get the save instance from the pk
    save_instance = get_object_or_404(Save, pk=pk)
    # Create a SaveAdmin instance
    save_admin = SaveAdmin(Save, admin.site)
    # Call the load_save method to load the save
    save_admin.load_save(request, queryset=[save_instance])
    return redirect("/pyscada_admin/saves/save/")


def download(request, pk):
    # Get the save instance from the pk
    save_instance = get_object_or_404(Save, pk=pk)
    # Create a SaveAdmin instance
    save_admin = SaveAdmin(Save, admin.site)
    # Call the download_save method to download the save
    response = save_admin.download_save(save_instance)
    return response


def upload(request):
    # Upload a save in the db_saves directory
    if request.method == "POST":
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES["file"]
            save_path = os.path.join(
                os.path.join(settings.BASE_DIR, "db_saves"), uploaded_file.name
            )
            # Write the save in the db_saves directory
            with open(save_path, "wb") as destination:
                for chunk in uploaded_file.chunks():
                    destination.write(chunk)

            # Refresh the save list
            messages.success(request, "Save successfully uploaded")
            return refresh(request)
    else:
        form = FileUploadForm()

    # Display the upload form
    return render(request, "upload.html", {"form": form})
