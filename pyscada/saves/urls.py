from django.urls import path
from . import views

app_name = "saves"

urlpatterns = [
    path("save-from-profil/<int:pk>/", views.save, name="save-from-profil"),
    path("refresh", views.refresh, name="save-refresh"),
    path("load/<int:pk>/", views.load, name="load"),
    path("download/<int:pk>/", views.download, name="download"),
    path("upload", views.upload, name="save-upload"),
]
