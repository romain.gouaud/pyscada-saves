# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db import connection
from django.apps import apps
from django.core.management.commands import dumpdata
from django.core import management
from django.conf import settings

import datetime
import gzip
import os
import logging

logger = logging.getLogger(__name__)


class Profil(models.Model):
    id = models.AutoField(primary_key=True)  # id field in database
    name = models.CharField(max_length=255, default="")  # name of the profil
    description = models.CharField(
        max_length=255, blank=True, null=True, default=""
    )  # description of the profil
    apps = models.TextField(
        default="", blank=True, null=True
    )  # apps to save (comma separated)
    tables = models.TextField(
        default="", blank=True, null=True
    )  # tables to save (comma separated)

    MODE_CHOICES = (("easy", "Easy"), ("advanced", "Advanced"))
    mode = models.CharField(max_length=255, choices=MODE_CHOICES, default="easy")

    @staticmethod
    def get_app_list():
        # Get the list of installed apps
        app_configs = apps.get_app_configs()
        app_list = [app_config.name.split(".")[-1] for app_config in app_configs]
        app_list.remove("saves")  # remove the saves app from the list
        return app_list

    @staticmethod
    def get_table_list():
        table_list = []
        apps_list = Profil.get_app_list()
        for app in apps_list:
            for model in apps.get_app_config(app).get_models():
                table_list.append(f"{app}.{model._meta.model_name}")
        return table_list

    def __str__(self):
        return self.name


class SavesScheduler(models.Model):
    id = models.AutoField(primary_key=True)  # id field in database
    name = models.CharField(max_length=255, default="")  # name of the scheduler
    time = models.TimeField(
        default=datetime.time(0, 0), help_text="Format: HH:MM"
    )  # time of the backup
    profil = models.ForeignKey(
        Profil, on_delete=models.PROTECT, default="1", null=True
    )  # profil to save
    active = models.BooleanField(default=False)  # backup shedule active
    PERIOD_CHOICES = (
        ("daily", "Daily"),
        ("pair days", "Pair days"),
        ("odd days", "Odd days"),
        ("weekly", "Weekly"),
        ("monthly", "Monthly"),
    )
    period = models.CharField(
        max_length=255, choices=PERIOD_CHOICES, default="daily"
    )  # backup shedule period

    def __str__(self):
        return self.name


class Save(models.Model):
    id = models.AutoField(primary_key=True)  # id field in database
    name = models.CharField(max_length=255, default="")  # name of the save
    profil = models.ForeignKey(
        Profil, on_delete=models.PROTECT, default="1", null=True
    )  # profil to save

    # Override the save_model method to create a save in db_saves
    def save(self, *args, **kwargs):
        def get_included_from_profil(profil):
            # ToDo : differenciate apps and tables

            if profil.tables:
                include = profil.tables.split(",")
            else:
                apps_to_save = profil.apps.split(",")
                include = []
                for app in apps_to_save:
                    include.append(app)
            return include

        # Create a save in db_saves
        SAVE_PATH = os.path.join(settings.BASE_DIR, "db_saves")

        name = (
            "db_save_"
            + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            + ".json.gz"
        )  # Name of the save
        save_path = os.path.join(SAVE_PATH, name)

        include = get_included_from_profil(self.profil)

        with gzip.open(save_path, "wt") as f:
            management.call_command(
                dumpdata.Command(), "--format=json", "--indent=2", include, stdout=f
            )
        # Save the name of the save in the database
        self.name = name
        super(Save, self).save(self, *args, **kwargs)

    def refresh_from_directory(self):
        # Get the list of saves from the db_saves directory
        SAVE_PATH = os.path.join(settings.BASE_DIR, "db_saves")
        saves = os.listdir(SAVE_PATH)
        # Get the list of saves from the database
        saves_from_db = Save.objects.all()
        saves_from_db_names = [save.name for save in saves_from_db]
        # Compare the two lists and delete the saves that are not in the db_saves directory
        for save in saves_from_db:
            if save.name not in saves:
                save.delete()
        # Compare the two lists and create the saves that are not in the database
        saves_to_create = []
        for save_name in saves:
            if save_name not in saves_from_db_names:
                saves_to_create.append(Save(name=save_name, profil=None))

        Save.objects.bulk_create(saves_to_create)

    def __str__(self):
        return self.name
