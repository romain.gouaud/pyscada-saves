# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib import messages
from django.forms.widgets import Textarea
from django.urls import reverse
from django.utils.html import format_html
from django.conf import settings
from django.http import HttpResponse

from .models import Profil, SavesScheduler, Save

import logging
import os
from typing import Any

from django.core import management
from django.core.management.commands import loaddata
from django.utils.translation import ngettext
from django.db.models.query import QuerySet
from django.http.request import HttpRequest

from pyscada.admin import admin_site

from django.core.serializers.base import DeserializationError

logger = logging.getLogger(__name__)

"""
    Admin site for the Profil model
"""


class ProfilForm(forms.ModelForm):
    # apps and tables fields are declared, displayed by default
    apps_list = forms.MultipleChoiceField(
        widget=FilteredSelectMultiple("verbose_name", is_stacked=False), required=False
    )
    tables_list = forms.MultipleChoiceField(
        widget=FilteredSelectMultiple("verbose_name", is_stacked=False), required=False
    )

    # raise error if no app or table is selected, depending of selected mode
    def clean(self):
        cleaned_data = super().clean()
        mode = cleaned_data.get("mode")
        apps_list = cleaned_data.get("apps_list")
        tables_list = cleaned_data.get("tables_list")

        if mode == "easy" and not apps_list:
            self.add_error("apps_list", "Please select at least one app.")
        elif mode == "advanced" and not tables_list:
            self.add_error("tables_list", "Please select at least one table.")

    class Meta:
        model = Profil
        fields = "__all__"
        # check mode field to display apps or tables fields
        widgets = {
            "mode": forms.Select(
                attrs={
                    "--hideshow-fields": "apps_list, tables_list,",  # hide by default
                    "--show-on-easy": "apps_list,",  # shown if mode is easy
                    "--show-on-advanced": "tables_list,",  # shown if mode is advanced
                }
            ),
        }

    class Media:
        js = ("pyscada/js/admin/hideshow.js",)

    # init value for apps and tables fields
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["apps_list"].choices = [(app, app) for app in Profil.get_app_list()]
        self.fields["tables_list"].choices = [
            (table, table) for table in Profil.get_table_list()
        ]

    # save apps and tables fields in the database
    def save(self, commit=True):
        instance = super().save(commit=False)
        if self.cleaned_data["mode"] == "easy":  # if mode is easy, save apps
            instance.apps = ",".join(self.cleaned_data["apps_list"])
            instance.tables = ""  # reset tables
        elif (
            self.cleaned_data["mode"] == "advanced"
        ):  # if mode is advanced, save tables
            instance.tables = ",".join(self.cleaned_data["tables_list"])
            instance.apps = ""  # reset apps

        if commit:
            instance.save()
        return instance


class ProfilAdmin(admin.ModelAdmin):
    """Display infos"""

    list_display = ("name", "profil_actions")
    exclude = (
        "apps",
        "tables",
    )  # by default, exclude apps and tables fields

    # display form if profil has no apps or tables
    def get_form(self, request, obj=None, **kwargs):
        if obj and (obj.apps or obj.tables):
            self.form = forms.ModelForm
        else:
            self.form = ProfilForm
        return super().get_form(request, obj, **kwargs)

    # display mode field if profil has no apps or tables
    def get_exclude(self, request, obj=None):
        exclude = super().get_exclude(request, obj) or ()
        if obj and (obj.apps or obj.tables):
            exclude += ("mode",)
        return exclude

    # manage profil apps and tables fields display
    def get_readonly_fields(self, request, obj=None):
        rof = super().get_readonly_fields(request, obj)
        if obj and obj.apps:  # if profil has apps
            rof += ("formatted_apps",)
        elif obj and obj.tables:  # if profil has tables
            rof += ("formatted_tables",)
        return rof

    """Display apps and tables fields in multilines"""

    # display readonly fields in multilines
    def formatted_apps(self, obj):
        return obj.apps.replace(",", "\n")

    def formatted_tables(self, obj):
        return obj.tables.replace(",", "\n")

    formatted_apps.short_description = "Apps"
    formatted_apps.widget = Textarea(attrs={"readonly": "readonly"})

    formatted_tables.short_description = "Tables"
    formatted_tables.widget = Textarea(attrs={"readonly": "readonly"})

    """Save button"""

    def profil_actions(self, obj):
        url = reverse("saves:save-from-profil", args=[obj.pk])
        return format_html('<a class="button" href="{}">Save</a>', url)

    profil_actions.short_description = "Save Actions"
    profil_actions.allow_tags = True


admin_site.register(Profil, ProfilAdmin)

"""
    Admin site for the Save model
"""


class SaveForm(forms.ModelForm):
    profil = forms.ModelChoiceField(
        queryset=Profil.objects.all()
    )  # drop-down list of the existing Profil

    class Meta:
        model = Save
        fields = "__all__"


class SaveAdmin(admin.ModelAdmin):
    # Override the default template to add refresh button
    change_list_template = "change_list.html"

    """Display infos"""
    # Display lines of the Save model in the admin site
    list_display = ("name", "profil_name", "save_actions")
    readonly_fields = ("name", "profil_name")

    # Display the form if the save has no profil
    def get_exclude(self, request, obj=None):
        exclude = super().get_exclude(request, obj) or ()
        if obj and obj.profil:
            exclude += ("profil",)
        return exclude

    # Display the name of the profil instead of the id
    def profil_name(self, obj):
        if obj.profil:
            name = obj.profil.name
        else:
            name = "Unknown"
        return name

    """Actions"""

    # ACTION : load the selected save
    @admin.action(
        description="Load the selected save (WARNING: Make sure to save the current database before loading a save)"
    )
    def load_save(self, request, queryset):
        SAVE_PATH = os.path.join(
            settings.BASE_DIR, "db_saves"
        )  # path to the saves directory
        updated = 0
        for save_obj in queryset:
            save_file = save_obj.name
            # Get the path to the save file
            save_file_path = os.path.join(SAVE_PATH, save_file)
            # use the loaddata command to load the save file
            try:
                management.call_command(loaddata.Command(), save_file_path, verbosity=1)
            except DeserializationError as e:
                self.message_user(
                    request,
                    "Deserialization error while loading save file : " + str(e),
                    messages.ERROR,
                )
                return
            except Exception as e:
                self.message_user(
                    request, "Error while loading save file : " + str(e), messages.ERROR
                )
                return
            updated += 1
        # display success message
        self.message_user(
            request,
            ngettext(
                "%d save was successfully loaded.",
                "%d saves were successfully loaded.",
                updated,
            )
            % updated,
            messages.SUCCESS,
        )

    """Delete overrides"""

    # ACTION : delete the selected save
    # Override the delete_queryset method to delete the save file from db_saves
    def delete_queryset(self, request: HttpRequest, queryset: QuerySet[Any]) -> None:
        # Delete the save file from db_saves
        SAVE_PATH = os.path.join(settings.BASE_DIR, "db_saves")
        for save_obj in queryset:
            save_file = save_obj.name
            save_path = os.path.join(SAVE_PATH, save_file)
            if os.path.exists(save_path):
                os.remove(save_path)
        return super().delete_queryset(request, queryset)

    # Add the update_save_list action to the admin site
    actions = [load_save]

    """Load and download button"""

    def save_actions(self, obj):
        load_url = reverse("saves:load", args=[obj.pk])
        download_url = reverse("saves:download", args=[obj.pk])

        actions_html = """
            <a class="button" href="{}">Load</a>
            <a class="button" href="{}">Download</a>
        """.format(
            load_url, download_url
        )

        return format_html(actions_html)

    save_actions.short_description = "Actions"
    save_actions.allow_tags = True

    """Download button"""

    def download_save(self, obj):
        # Get the path to the save file
        SAVE_PATH = os.path.join(settings.BASE_DIR, "db_saves")
        save_file = obj.name
        SAVE_PATH = os.path.join(SAVE_PATH, save_file)
        # Open the file for reading content
        with open(SAVE_PATH, "rb") as file:
            # Create the HttpResponse object with the object as content
            response = HttpResponse(
                file.read(), content_type="application/octet-stream"
            )
            # Specify the filename
            response["Content-Disposition"] = 'attachment; filename="{}"'.format(
                obj.name
            )
            return response


admin_site.register(Save, SaveAdmin)

"""
    Admin site for the scheduler model
"""


class SavesSchedulerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "time",
        "period",
        "profil",
        "active",
    )  # add time field
    list_editable = ("active",)


admin_site.register(SavesScheduler, SavesSchedulerAdmin)
