PyScada Save Extension
===========================

This is an extension for PyScada to manage database save from the admin interface. 
(extras/sample_script).


What is Working
---------------

- Manage profils
- Manage saves


What is not Working/Missing
---------------------------

 - schedule save
 - documentation

Installation
------------

 - pip install pyscada-scripting

License
-------

The project is licensed under the _GNU AFFERO GENERAL PUBLIC LICENSE Version 3 (AGPLv3)_.
-
